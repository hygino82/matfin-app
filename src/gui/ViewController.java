package gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class ViewController implements Initializable {

	private int currentValue;

	@FXML
	private RadioButton rbPrice, rbSac;

	@FXML
	private TextField txtValor;

	@FXML
	private TextField txtTaxa;

	@FXML
	private Spinner<Integer> spParcelas;

	@FXML
	private Button btCalcula;

	@FXML
	private TextArea txtResultado;

	@FXML
	public void btCalculaAction() {
		try {
			double pv = Double.parseDouble(txtValor.getText());
			double taxaPercentual = Double.parseDouble(txtTaxa.getText());
			double i = taxaPercentual / 100.0;
			int n = currentValue;
			double saldo = pv;

			txtResultado.clear();
			txtResultado.appendText("\nPV = " + String.format("%.2f", pv));
			txtResultado.appendText("\ni = " + String.format("%.2f", i));
			txtResultado.appendText("\nn = " + String.format("%d", n));

			if (rbPrice.isSelected()) {
				double pmt = pv * Math.pow(1 + i, n) * i / (Math.pow(1 + i, n) - 1);
				txtResultado.appendText("\nPMT = " + String.format("%.2f", pmt));

				double totalParcelas = pmt * n;
				txtResultado.appendText("\nTOTAL = " + String.format("%.2f", totalParcelas));

				double diferenca = totalParcelas - pv;
				txtResultado.appendText("\nTotal - Saldo = " + String.format("%.2f", diferenca));

			}

		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 120);

		valueFactory.setValue(10);

		spParcelas.setValueFactory(valueFactory);

		currentValue = spParcelas.getValue();

		spParcelas.valueProperty().addListener(new ChangeListener<Integer>() {

			@Override
			public void changed(ObservableValue<? extends Integer> arg0, Integer arg1, Integer arg2) {

				currentValue = spParcelas.getValue();
			}
		});
	}
}
